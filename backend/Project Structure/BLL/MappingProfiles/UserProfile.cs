﻿using AutoMapper;
using Common.DTOs.User;
using DAL.Entities;

namespace BLL.MappingProfiles
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<UserEntity, UserDTO>();
            CreateMap<UserDTO, UserEntity>();

            CreateMap<UserCreateDTO, UserEntity>();
        }
    }
}
