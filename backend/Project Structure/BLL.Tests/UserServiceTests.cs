﻿using AutoMapper;
using BLL.Exceptions;
using BLL.Interfaces;
using BLL.MappingProfiles;
using BLL.Services;
using Common.DTOs.Team;
using Common.DTOs.User;
using DAL.Entities;
using DAL.UnitOfWork.Implementation;
using DAL.UnitOfWork.Interfaces;
using FakeItEasy;
using System;
using System.Threading.Tasks;
using Xunit;
using System.Reflection;
using System.Linq;
using DAL.Context;
using Microsoft.EntityFrameworkCore;

namespace BLL.Tests
{
    public class UserServiceTests : IClassFixture<SeedDataFixture>, IDisposable
    {
        private readonly IUserService _userService;
        private readonly Mapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        private readonly SeedDataFixture _fixture;

        public UserServiceTests()
        {
            _mapper = new Mapper(new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<TeamProfile>();
                cfg.AddProfile<UserProfile>();
                cfg.AddProfile<ProjectProfile>();
                cfg.AddProfile<TaskProfile>();
            }));
            _fixture = new SeedDataFixture();

            _unitOfWork = new UnitOfWork(_fixture.Context);

            var teamService = A.Fake<ITeamService>();

            _userService = new UserService(_unitOfWork, _mapper, teamService);
        }

        public void Dispose()
        {
            _fixture.Dispose();
            _unitOfWork.Dispose();
            _userService.Dispose();
        }
        

        [Fact]
        public async Task GetUserInfo_WhenTwoProjects_ThenLastProjectIsLastCreated()
        {
            _fixture.USER_COUNT = 0;
            _fixture.PROJECT_COUNT = 0;
            _fixture.TASK_COUNT = 0;
            _fixture.Seed();

            await _userService.CreateAsync(A.Fake<UserCreateDTO>());

            var projectRepository = _unitOfWork.Set<ProjectEntity>();

            var project1 = A.Fake<ProjectEntity>();
            project1.CreatedAt = DateTime.Now;
            project1.AuthorId = 1;
            project1.TeamId = 1;
            await projectRepository.CreateAsync(project1);

            var project2 = A.Fake<ProjectEntity>();
            project2.CreatedAt = DateTime.Now.AddDays(-1);
            project2.AuthorId = 1;
            project2.TeamId = 1;
            await projectRepository.CreateAsync(project2);

            await _unitOfWork.SaveChangesAsync();

            Assert.Equal(project1.Id, (await _userService.GetUserInfo(1)).LastProject.Id);
        }

        [Fact]
        public async Task GetUserInfo_WhenOneProjectOneUser10Tasks_ThenLastProjectTaskCountIs10()
        {
            _fixture.TASK_COUNT = 10;
            _fixture.Seed();

            Assert.Equal(10, (await _userService.GetUserInfo(1)).LastProjectTasksCount);
        }

        [Fact]
        public async Task GetUserInfo_WhenNonExistingUser_ThenThrowNotFoundException()
        {
            await Assert.ThrowsAsync<NotFoundException>(() => _userService.GetUserInfo(0));
        }

        [Fact]
        public async Task CreateAsync_WhenNewUser_TnenUsersPlusOne()
        {
            await _userService.CreateAsync(A.Fake<UserCreateDTO>());

            Assert.Single(await _userService.GetAllAsync());
        }

        [Fact]
        public async Task CreateAsync_WhenCreateUser_TnenCallToRepositoryCreateAsync()
        {
            var unitOfWorkFake = A.Fake<IUnitOfWork>();
            var teamServiceFake = A.Fake<ITeamService>();

            var userService = new UserService(unitOfWorkFake, _mapper, teamServiceFake);

            var repository = (IRepository<UserEntity>)typeof(UserService).GetFields(BindingFlags.NonPublic | BindingFlags.Instance)
                                .FirstOrDefault(f => f.Name == "_repository")
                                .GetValue(userService);

            await userService.CreateAsync(A.Fake<UserCreateDTO>());

            A.CallTo(() => repository.CreateAsync(A<UserEntity>.Ignored)).MustHaveHappened(1, Times.Exactly);
        }

        [Fact]
        public async Task CreateAsync_WhenNewTeamAndAddUserToTeam_TnenTeamUsersPlusOne()
        {
            var teamService = new TeamService(_unitOfWork, _mapper);
            var teamCreated = await teamService.CreateAsync(A.Fake<TeamCreateDTO>());

            var userDTO = A.Fake<UserCreateDTO>();
            userDTO.TeamId = teamCreated.Id;
            await _userService.CreateAsync(userDTO);

            var team = await teamService.GetByIdAsync(teamCreated.Id);

            Assert.Single(team.Users);
        }
    }
}
