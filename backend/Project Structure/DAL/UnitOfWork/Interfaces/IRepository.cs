﻿
using DAL.Entities.Abstract;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DAL.UnitOfWork.Interfaces
{
    public interface IRepository<TEntity> where TEntity : BaseEntity
    {
        Task<IQueryable<TEntity>> GetAllAsync();

        Task<TEntity> GetByIdAsync(int id);

        Task CreateAsync(TEntity entity);

        Task UpdateAsync(TEntity entity);

        Task DeleteByIdAsync(int id);
    }
}
