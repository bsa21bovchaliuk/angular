﻿using DAL.Context;
using DAL.Entities.Abstract;
using DAL.UnitOfWork.Interfaces;
using System.Linq;
using System.Threading.Tasks;

namespace DAL.UnitOfWork.Implementation
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : BaseEntity
    {
        protected readonly ProjectContext _context;

        public Repository(ProjectContext context)
        {
            _context = context;
        }

        public virtual Task CreateAsync(TEntity entity)
        {
            return _context.Set<TEntity>().AddAsync(entity).AsTask();
        }

        public virtual async Task DeleteByIdAsync(int id)
        {

            var dbset = _context.Set<TEntity>();
            var entity = await dbset.FindAsync(id);
            if (_context.Entry(entity).State == Microsoft.EntityFrameworkCore.EntityState.Detached)
            {
                dbset.Attach(entity);
            }
            dbset.Remove(entity);
        }

        public virtual Task<IQueryable<TEntity>> GetAllAsync()
        {
            return Task.Run(() => _context.Set<TEntity>().AsQueryable());
        }

        public virtual Task<TEntity> GetByIdAsync(int id)
        {
            return _context.Set<TEntity>().FindAsync(id).AsTask();
        }

        public virtual Task UpdateAsync(TEntity entity)
        {
            return Task.Run(() =>
            {
                _context.Set<TEntity>().Attach(entity);
                _context.Entry(entity).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            });
        }
    }
}
