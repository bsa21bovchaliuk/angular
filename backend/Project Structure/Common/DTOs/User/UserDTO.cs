﻿using Common.DTOs.Project;
using Common.DTOs.Task;
using Common.DTOs.Team;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Common.DTOs.User
{
    public class UserDTO
    {
        public int Id { get; set; }

        public int? TeamId { get; set; }

        public TeamDTO Team { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public DateTime Birthday { get; set; }

        public DateTime RegisteredAt { get; set; }

        public override string ToString()
        {
            return $"{"id:",-Constants.Indent}{Id}\n{"firstName:",-Constants.Indent}{FirstName}\n"
                + $"{"lastName:",-Constants.Indent}{LastName}\n{"email:",-Constants.Indent}{Email}\n"
                + $"{"birthday:",-Constants.Indent}{Birthday}\n{"registeredAt:",-Constants.Indent}{RegisteredAt}\n"
                + $"{"teamId:",-Constants.Indent}{TeamId}\n";
        }

        public string ToString(string indent)
        {
            return Regex.Replace(ToString(), @"(?m)^", indent);
        }
    }
}
