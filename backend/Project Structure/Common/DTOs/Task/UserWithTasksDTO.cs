﻿using Common.DTOs.User;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.DTOs.Task
{
    public class UserWithTasksDTO
    {
        public UserDTO User { get; set; }
        public IEnumerable<TaskDTO> Tasks { get; set; }

        public override string ToString()
        {
            return $"{User}"
                + $"tasks:\n{Tasks.ToList().Aggregate(new StringBuilder(), (current, next) => current.Append($"{next.ToString($"{"",-Constants.Indent}")}\n")).ToString()}";
        }
    }
}
