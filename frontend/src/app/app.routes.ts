import { Routes } from '@angular/router';
import { ProjectListComponent } from './modules/project/components/project-list/project-list.component';
import { ProjectNewEditComponent } from './modules/project/components/project-new-edit/project-new-edit.component';
import { ProjectInfoComponent } from './modules/project/components/project-info/project-info.component';
import { PendingChangesGuard } from './modules/shared/guards/pending-changes.guard';
import { UserListComponent } from './modules/user/components/user-list/user-list.component';
import { UserNewEditComponent } from './modules/user/components/user-new-edit/user-new-edit.component';
import { UserInfoComponent } from './modules/user/components/user-info/user-info.component';
import { TeamListComponent } from './modules/team/components/team-list/team-list.component';
import { TeamNewEditComponent } from './modules/team/components/team-new-edit/team-new-edit.component';
import { TeamInfoComponent } from './modules/team/components/team-info/team-info.component';
import { TaskListComponent } from './modules/task/components/task-list/task-list.component';
import { TaskNewEditComponent } from './modules/task/components/task-new-edit/task-new-edit.component';
import { TaskInfoComponent } from './modules/task/components/task-info/task-info.component';


export const AppRoutes: Routes = [
    { path: 'projects', component: ProjectListComponent, children: [
        { path: 'new', component: ProjectNewEditComponent, canDeactivate: [PendingChangesGuard] },
        { path: ':id', component: ProjectInfoComponent },
        { path: ':id/edit', component: ProjectNewEditComponent, canDeactivate: [PendingChangesGuard] }
    ]},
    { path: 'users', component: UserListComponent, children: [
        { path: 'new', component: UserNewEditComponent, canDeactivate: [PendingChangesGuard] },
        { path: ':id', component: UserInfoComponent },
        { path: ':id/edit', component: UserNewEditComponent, canDeactivate: [PendingChangesGuard] }
    ]},
    { path: 'teams', component: TeamListComponent, children: [
        { path: 'new', component: TeamNewEditComponent, canDeactivate: [PendingChangesGuard] },
        { path: ':id', component: TeamInfoComponent },
        { path: ':id/edit', component: TeamNewEditComponent, canDeactivate: [PendingChangesGuard] }
    ]},
    { path: 'tasks', component: TaskListComponent, children: [
        { path: 'new', component: TaskNewEditComponent, canDeactivate: [PendingChangesGuard] },
        { path: ':id', component: TaskInfoComponent },
        { path: ':id/edit', component: TaskNewEditComponent, canDeactivate: [PendingChangesGuard] }
    ]},
    { path: '**', redirectTo: 'projects' }
];
