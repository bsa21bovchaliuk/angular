import { Component, OnInit, OnDestroy } from '@angular/core';
import { ComponentCanDeactivate } from 'src/app/modules/shared/guards/pending-changes.guard';
import { User } from 'src/app/modules/shared/models/user/user';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Subject, Observable } from 'rxjs';
import { Team } from 'src/app/modules/shared/models/team/team';
import { UserService } from 'src/app/modules/shared/services/user.service';
import { TeamService } from 'src/app/modules/shared/services/team.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { UserListComponent } from '../user-list/user-list.component';
import { SnackBarService } from 'src/app/modules/shared/services/snack-bar.service';
import { CustomValidators } from 'src/app/modules/shared/validators/custom-validators';
import { takeUntil } from 'rxjs/operators';
import { UpdateUser } from 'src/app/modules/shared/models/user/update-user';
import { NewUser } from 'src/app/modules/shared/models/user/new-user';

@Component({
  selector: 'app-user-new-edit',
  templateUrl: './user-new-edit.component.html',
  styleUrls: ['./user-new-edit.component.css']
})
export class UserNewEditComponent implements OnInit, OnDestroy, ComponentCanDeactivate {
  public user: User = {} as User;
  public teams: Team[];
  public userForm: FormGroup;

  public isEdit = false;
  public isSaved = false;

  private unsubscribe$ = new Subject<void>();
  constructor(
    private teamService: TeamService,
    private userService: UserService,
    private router: Router,
    private route: ActivatedRoute,
    private userListComponent: UserListComponent,
    private snackBarService: SnackBarService
    ) { }
  public canDeactivate(): boolean | Observable<boolean> {
    return !this.userForm.dirty || this.isSaved;
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  ngOnInit(): void {
    this.getTeams();
    const dateMax = new Date();
    dateMax.setFullYear(dateMax.getFullYear() - 18);

    this.userForm = new FormGroup({
      firstName: new FormControl(this.user.firstName, [
          Validators.required,
          Validators.minLength(4),
          Validators.maxLength(20)
        ]),
        lastName: new FormControl(this.user.lastName, [
          Validators.required,
          Validators.minLength(4),
          Validators.maxLength(20)
        ]),
        email: new FormControl(this.user.email, [
          Validators.required,
          Validators.email,
          Validators.maxLength(50)
        ]),
        birthday: new FormControl('', [
          Validators.required,
          CustomValidators.dateMaximum(dateMax.toLocaleDateString('en-CA'))
        ]),
        team: new FormControl(this.teams)
    });

    this.route.params
      .subscribe(
        (params: Params) => {
            const id = +params[`id`];
            this.isEdit = Boolean(id);
            if (this.isEdit){
              this.getUser(+params[`id`]);
            }
        }
      );
  }

  public getUser(id: number): void {
    this.userService
      .getUser(id)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        (resp) => {
          this.user = resp.body;
          this.userForm.controls[`birthday`]
                .setValue(
                  new Date(this.user.birthday)
                    .toLocaleDateString('en-CA')
                );
        },
        (error) => {
          this.snackBarService.showErrorMessage(error.message);
          this.onCancel();
        }
      );
  }

  public getTeams(): void {
    this.teamService
      .getTeams()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        (resp) => {
          this.teams = resp.body;
        },
        (error) => {
          this.snackBarService.showErrorMessage(error.message);
          this.onCancel();
        }
      );
  }

  public onSubmit(): void{
    if (this.isEdit){
      this.userService
        .updateUser(this.user as UpdateUser)
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe(
          (resp) => {
            const index = this.userListComponent.users.findIndex(p => p.id === resp.body.id);
            this.userListComponent.users[index] = resp.body;
            this.isSaved = true;
          },
          (error) => (this.snackBarService.showErrorMessage(error.message))
        )
        .add(() => (this.onCancel()));
    }
    else {
      this.userService
      .createUser(this.user as NewUser)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        (resp) => {
            this.userListComponent.users.push(resp.body);
            this.isSaved = true;
            this.onSubmitNavigate(resp.body.id);
        },
        (error) => {
          this.snackBarService.showErrorMessage(error.message);
          this.onCancel();
        }
      );
    }
  }

  public onSubmitNavigate(id: number): void{
    this.router.navigate([`../${id}`], {relativeTo: this.route});
  }

  public onCancel(): void{
    this.router.navigate(['../'], {relativeTo: this.route});
  }
}
