import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { User } from 'src/app/modules/shared/models/user/user';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-user-link',
  templateUrl: './user-link.component.html',
  styleUrls: ['./user-link.component.css']
})
export class UserLinkComponent implements OnDestroy {
  @Input() user: User;

  private unsubscribe$ = new Subject<void>();

  constructor() { }
  public ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
