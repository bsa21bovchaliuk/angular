import { Component, OnInit, OnDestroy } from '@angular/core';
import { User } from 'src/app/modules/shared/models/user/user';
import { Subject } from 'rxjs';
import { UserService } from 'src/app/modules/shared/services/user.service';
import { Router, ActivatedRoute } from '@angular/router';
import { SnackBarService } from 'src/app/modules/shared/services/snack-bar.service';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit, OnDestroy {
  public users: User[] = [];
  public loading = false;

  private unsubscribe$ = new Subject<void>();

  constructor(
    private userService: UserService,
    private router: Router,
    private route: ActivatedRoute,
    private snackBarService: SnackBarService
  ) { }

  public ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  public ngOnInit(): void {
    this.getUsers();
  }

  public getUsers(): void {
    this.loading = true;
    this.userService
      .getUsers()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        (resp) => {
          this.users = resp.body;
        },
        (error) => (this.snackBarService.showErrorMessage(error.message))
      )
      .add(() => (this.loading = false));
  }

  public newUser(): void{
    this.router.navigate(['new'], {relativeTo: this.route});
  }
}
