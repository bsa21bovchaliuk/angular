import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { ProjectService } from 'src/app/modules/shared/services/project.service';
import { Project } from 'src/app/modules/shared/models/project/project';
import { SnackBarService } from 'src/app/modules/shared/services/snack-bar.service';

@Component({
  selector: 'app-project-list',
  templateUrl: './project-list.component.html',
  styleUrls: ['./project-list.component.css']
})
export class ProjectListComponent implements OnInit, OnDestroy {
  public projects: Project[] = [];
  public loading = false;

  private unsubscribe$ = new Subject<void>();

  constructor(
    private projectService: ProjectService,
    private router: Router,
    private route: ActivatedRoute,
    private snackBarService: SnackBarService
  ) { }

  public ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  public ngOnInit(): void {
    this.getProjects();
  }

  public getProjects(): void {
    this.loading = true;
    this.projectService
      .getProjects()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        (resp) => {
          this.projects = resp.body;
        },
        (error) => (this.snackBarService.showErrorMessage(error.message))
      )
      .add(() => (this.loading = false));
  }

  public newProject(): void{
    this.router.navigate(['new'], {relativeTo: this.route});
  }
}
