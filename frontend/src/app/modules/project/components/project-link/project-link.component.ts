import { Component, OnDestroy, Input } from '@angular/core';
import { Subject } from 'rxjs';
import { Project } from 'src/app/modules/shared/models/project/project';

@Component({
  selector: 'app-project-link',
  templateUrl: './project-link.component.html',
  styleUrls: ['./project-link.component.css']
})
export class ProjectLinkComponent implements OnDestroy {
  @Input() project: Project;

  private unsubscribe$ = new Subject<void>();

  constructor() { }
  public ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
