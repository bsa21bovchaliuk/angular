import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { takeUntil } from 'rxjs/operators';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ProjectListComponent } from '../project-list/project-list.component';
import { ProjectService } from 'src/app/modules/shared/services/project.service';
import { TeamService } from 'src/app/modules/shared/services/team.service';
import { UserService } from 'src/app/modules/shared/services/user.service';
import { User } from 'src/app/modules/shared/models/user/user';
import { Project } from 'src/app/modules/shared/models/project/project';
import { Team } from 'src/app/modules/shared/models/team/team';
import { UpdateProject } from 'src/app/modules/shared/models/project/update-project';
import { NewProject } from 'src/app/modules/shared/models/project/new-project';
import { ComponentCanDeactivate } from 'src/app/modules/shared/guards/pending-changes.guard';
import { CustomValidators } from '../../../shared/validators/custom-validators';
import { SnackBarService } from 'src/app/modules/shared/services/snack-bar.service';

@Component({
  selector: 'app-project-new-edit',
  templateUrl: './project-new-edit.component.html',
  styleUrls: ['./project-new-edit.component.css']
})
export class ProjectNewEditComponent implements OnInit, OnDestroy, ComponentCanDeactivate {
  public project: Project = {} as Project;
  public teams: Team[];
  public users: User[];
  public projectForm: FormGroup;

  public isEdit = false;
  public isSaved = false;

  private unsubscribe$ = new Subject<void>();
  constructor(
    private projectService: ProjectService,
    private teamService: TeamService,
    private userService: UserService,
    private router: Router,
    private route: ActivatedRoute,
    private projectListComponent: ProjectListComponent,
    private snackBarService: SnackBarService
    ) { }
  public canDeactivate(): boolean | Observable<boolean> {
    return !this.projectForm.dirty || this.isSaved;
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  ngOnInit(): void {
    this.getTeams();
    this.getUsers();

    this.projectForm = new FormGroup({
      name: new FormControl(this.project.name, [
          Validators.required,
          Validators.minLength(4),
          Validators.maxLength(100)
        ]),
        description: new FormControl(this.project.description),
        deadline: new FormControl('', [
          Validators.required,
          CustomValidators.dateMinimum(new Date().toLocaleDateString('en-CA'))
        ]),
        team: new FormControl(this.teams, Validators.required),
        author: new FormControl(this.users, Validators.required)
    });

    this.route.params
      .subscribe(
        (params: Params) => {
            const id = +params[`id`];
            this.isEdit = Boolean(id);
            if (this.isEdit){
              this.getProject(+params[`id`]);
            }
        }
      );
  }

  public getProject(id: number): void {
    this.projectService
      .getProject(id)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        (resp) => {
          this.project = resp.body;
          this.projectForm.controls[`deadline`]
                .setValue(
                  new Date(this.project.deadline)
                    .toLocaleDateString('en-CA')
                );
        },
        (error) => {
          this.snackBarService.showErrorMessage(error.message);
          this.onCancel();
        }
      );
  }

  public getUsers(): void {
    this.userService
      .getUsers()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        (resp) => {
          this.users = resp.body;
        },
        (error) => {
          this.snackBarService.showErrorMessage(error.message);
          this.onCancel();
        }
      );
  }

  public getTeams(): void {
    this.teamService
      .getTeams()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        (resp) => {
          this.teams = resp.body;
        },
        (error) => {
          this.snackBarService.showErrorMessage(error.message);
          this.onCancel();
        }
      );
  }

  public onSubmit(): void{
    if (this.isEdit){
      this.projectService
        .updateProject(this.project as UpdateProject)
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe(
          (resp) => {
            const index = this.projectListComponent.projects.findIndex(p => p.id === resp.body.id);
            this.projectListComponent.projects[index] = resp.body;
            this.isSaved = true;
          },
          (error) => (this.snackBarService.showErrorMessage(error.message))
        )
        .add(() => (this.onCancel()));
    }
    else {
      this.projectService
      .createProject(this.project as NewProject)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        (resp) => {
            this.projectListComponent.projects.push(resp.body);
            this.isSaved = true;
            this.onSubmitNavigate(resp.body.id);
        },
        (error) => {
          this.snackBarService.showErrorMessage(error.message);
          this.onCancel();
        }
      );
    }
  }

  public onSubmitNavigate(id: number): void{
    this.router.navigate([`../${id}`], {relativeTo: this.route});
  }

  public onCancel(): void{
    this.router.navigate(['../'], {relativeTo: this.route});
  }
}
