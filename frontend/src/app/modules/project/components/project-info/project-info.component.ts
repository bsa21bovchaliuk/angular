import { Component, OnInit, OnDestroy, Input, Output } from '@angular/core';
import { Subject } from 'rxjs';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { takeUntil } from 'rxjs/operators';
import { ProjectListComponent } from '../project-list/project-list.component';
import { ProjectService } from 'src/app/modules/shared/services/project.service';
import { Project } from 'src/app/modules/shared/models/project/project';
import { SnackBarService } from '../../../shared/services/snack-bar.service';

@Component({
  selector: 'app-project-info',
  templateUrl: './project-info.component.html',
  styleUrls: ['./project-info.component.css']
})
export class ProjectInfoComponent implements OnDestroy, OnInit {
  public project: Project;

  private unsubscribe$ = new Subject<void>();

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private projectService: ProjectService,
    private projectListComponent: ProjectListComponent,
    private snackBarService: SnackBarService
    ) { }
  ngOnInit(): void {
    this.route.params
      .subscribe(
        (params: Params) => {
            const id: string = params[`id`];
            this.getProject(+id);
            this.scroll(id);
        }
      );
  }

  public scroll(id: string): void{
    const element = document.getElementById(id);
    const offset = element.getBoundingClientRect().top - document.body.getBoundingClientRect().top - 165;
    window.scrollTo({
      top: offset,
      behavior: 'smooth'
    });
  }

  public getProject(id: number): void {
    this.projectService
      .getProject(id)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        (resp) => {
          this.project = resp.body;
        },
        (error) => {
          this.snackBarService.showErrorMessage(error.message);
          this.router.navigate(['../'], {relativeTo: this.route});
        }
      );
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  public onEditProject(): void{
    this.router.navigate(['edit'], {relativeTo: this.route});
  }

  public onDeleteProject(): void{
    this.projectService
      .deleteProject(this.project.id)
      .pipe(takeUntil(this.unsubscribe$))
        .subscribe(
          (resp) => {
            this.projectListComponent.projects = this.projectListComponent.projects.filter(p => p.id !== this.project.id);
            this.snackBarService.showUsualMessage(`Project(id: ${this.project.id}) deleted`);
          },
          (error) => (this.snackBarService.showErrorMessage(error.message))
        )
        .add(() => (this.router.navigate(['../'], {relativeTo: this.route})));
  }
}
