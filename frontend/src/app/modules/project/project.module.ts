import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProjectInfoComponent } from './components/project-info/project-info.component';
import { ProjectLinkComponent } from './components/project-link/project-link.component';
import { ProjectListComponent } from './components/project-list/project-list.component';
import { ProjectNewEditComponent } from './components/project-new-edit/project-new-edit.component';
import { SharedModule } from '../shared/shared.module';
import { RouterModule } from '@angular/router';
import { AppRoutes } from '../../app.routes';

@NgModule({
  declarations: [
    ProjectInfoComponent,
    ProjectLinkComponent,
    ProjectListComponent,
    ProjectNewEditComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(AppRoutes)
  ]
})
export class ProjectModule { }
