import { PipeTransform, Pipe } from '@angular/core';

@Pipe({
    name: 'localizedDate',
    pure: false
  })
  export class LocalizedDatePipe implements PipeTransform {

      transform(value: Date): string {

        const dateOptions = {
          year: 'string',
          month: 'string',
          day: 'string'
        };

        const date = new Date(value).toLocaleDateString('uk');
        const index = date.lastIndexOf('р');

        return date.slice(0, index - 1);
    }
}
