import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialComponentsModule } from './components/common/material-components.module';
import { DropdownDirective } from './directives/dropdown.directive';
import { HomeComponent } from './components/home/home.component';
import { LocalizedDatePipe } from './pipes/localized-date-pipe';
import { StringTaskStatePipe } from './pipes/task-state.pipe';
import { KeysPipe } from './pipes/keys.pipe';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { AppRoutes } from 'src/app/app.routes';
import { PendingChangesGuard } from './guards/pending-changes.guard';
import { TaskStatusDirective } from './directives/task-status.directive';

@NgModule({
  declarations: [
    HomeComponent,
    DropdownDirective,
    LocalizedDatePipe,
    StringTaskStatePipe,
    KeysPipe,
    TaskStatusDirective
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(AppRoutes),
    MaterialComponentsModule
  ],
  exports: [
    MaterialComponentsModule,
    FormsModule,
    ReactiveFormsModule,
    DropdownDirective,
    LocalizedDatePipe,
    StringTaskStatePipe,
    KeysPipe,
    TaskStatusDirective,
    HomeComponent
  ],
  providers: [
    PendingChangesGuard
  ]
})
export class SharedModule { }
