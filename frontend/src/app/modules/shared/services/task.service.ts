import { Injectable } from '@angular/core';
import { HttpInternalService } from './http-internal.service';
import { Task } from '../models/task/task';
import { NewTask } from '../models/task/new-task';
import { UpdateTask } from '../models/task/update-task';
import { Observable } from 'rxjs';
import { HttpResponse } from '@angular/common/http';

@Injectable({ providedIn: 'root' })
export class TaskService {
    public routePrefix = '/api/tasks';

    constructor(private httpService: HttpInternalService) { }

    public getTasks(): Observable<HttpResponse<Task[]>> {
        return this.httpService.getFullRequest<Task[]>(`${this.routePrefix}`);
    }

    public getTask(id: number): Observable<HttpResponse<Task>> {
        return this.httpService.getFullRequest<Task>(`${this.routePrefix}/${id}`);
    }

    public createTask(task: NewTask): Observable<HttpResponse<Task>> {
        return this.httpService.postFullRequest<Task>(`${this.routePrefix}`, task);
    }

    public updateTask(task: UpdateTask): Observable<HttpResponse<Task>> {
        return this.httpService.putFullRequest<Task>(`${this.routePrefix}`, task);
    }

    public deleteTask(id: number): Observable<HttpResponse<unknown>> {
        return this.httpService.deleteFullRequest(`${this.routePrefix}/${id}`);
    }
}
