import { Injectable } from '@angular/core';
import { HttpInternalService } from './http-internal.service';
import { Team } from '../models/team/team';
import { NewTeam } from '../models/team/new-team';
import { UpdateTeam } from '../models/team/update-team';
import { Observable } from 'rxjs';
import { HttpResponse } from '@angular/common/http';

@Injectable({ providedIn: 'root' })
export class TeamService {
    public routePrefix = '/api/teams';

    constructor(private httpService: HttpInternalService) { }

    public getTeams(): Observable<HttpResponse<Team[]>> {
        return this.httpService.getFullRequest<Team[]>(`${this.routePrefix}`);
    }

    public getTeam(id: number): Observable<HttpResponse<Team>> {
        return this.httpService.getFullRequest<Team>(`${this.routePrefix}/${id}`);
    }

    public createTeam(team: NewTeam): Observable<HttpResponse<Team>> {
        return this.httpService.postFullRequest<Team>(`${this.routePrefix}`, team);
    }

    public updateTeam(team: UpdateTeam): Observable<HttpResponse<Team>> {
        return this.httpService.putFullRequest<Team>(`${this.routePrefix}`, team);
    }

    public deleteTeam(id: number): Observable<HttpResponse<unknown>> {
        return this.httpService.deleteFullRequest(`${this.routePrefix}/${id}`);
    }
}
