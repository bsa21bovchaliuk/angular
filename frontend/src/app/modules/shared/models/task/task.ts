import { User } from '../user/user';
import { TaskState } from '../enums/taskState';
import { Project } from '../project/project';

export interface Task {
    id: number;
    projectId: number;
    project: Project;
    performerId: number;
    performer: User;
    name: string;
    description: string;
    createdAt: Date;
    finishedAt: Date;
    state: TaskState;
}
