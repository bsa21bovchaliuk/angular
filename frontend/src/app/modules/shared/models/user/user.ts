import { Team } from '../team/team';
import { Task } from '../task/task';
import { Project } from '../project/project';

export interface User {
    id: number;
    teamId: number | null;
    team: Team;
    firstName: string;
    lastName: string;
    email: string;
    birthday: Date;
    registeredAt: Date;
}
