import { Component, OnInit, OnDestroy } from '@angular/core';
import { Task } from 'src/app/modules/shared/models/task/task';
import { Subject } from 'rxjs';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { TaskService } from 'src/app/modules/shared/services/task.service';
import { TaskListComponent } from '../task-list/task-list.component';
import { SnackBarService } from 'src/app/modules/shared/services/snack-bar.service';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-task-info',
  templateUrl: './task-info.component.html',
  styleUrls: ['./task-info.component.css']
})
export class TaskInfoComponent implements OnInit, OnDestroy {
  public task: Task;

  private unsubscribe$ = new Subject<void>();

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private taskService: TaskService,
    private taskListComponent: TaskListComponent,
    private snackBarService: SnackBarService
    ) { }
  ngOnInit(): void {
    this.route.params
      .subscribe(
        (params: Params) => {
            const id: string = params[`id`];
            this.getTask(+id);
            this.scroll(id);
        }
      );
  }

  public scroll(id: string): void{
    const element = document.getElementById(id);
    const offset = element.getBoundingClientRect().top - document.body.getBoundingClientRect().top - 165;
    window.scrollTo({
      top: offset,
      behavior: 'smooth'
    });
  }

  public getTask(id: number): void {
    this.taskService
      .getTask(id)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        (resp) => {
          this.task = resp.body;
        },
        (error) => {
          this.snackBarService.showErrorMessage(error.message);
          this.router.navigate(['../'], {relativeTo: this.route});
        }
      );
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  public onEditTask(): void{
    this.router.navigate(['edit'], {relativeTo: this.route});
  }

  public onDeleteTask(): void{
    this.taskService
      .deleteTask(this.task.id)
      .pipe(takeUntil(this.unsubscribe$))
        .subscribe(
          (resp) => {
            this.taskListComponent.tasks = this.taskListComponent.tasks.filter(p => p.id !== this.task.id);
            this.snackBarService.showUsualMessage(`Task(id: ${this.task.id}) deleted`);
          },
          (error) => (this.snackBarService.showErrorMessage(error.message))
        )
        .add(() => (this.router.navigate(['../'], {relativeTo: this.route})));
  }
}
