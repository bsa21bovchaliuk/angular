import { Component, OnInit, OnDestroy } from '@angular/core';
import { ComponentCanDeactivate } from 'src/app/modules/shared/guards/pending-changes.guard';
import { Task } from 'src/app/modules/shared/models/task/task';
import { User } from 'src/app/modules/shared/models/user/user';
import { Project } from 'src/app/modules/shared/models/project/project';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ProjectService } from 'src/app/modules/shared/services/project.service';
import { Subject, Observable } from 'rxjs';
import { TaskService } from 'src/app/modules/shared/services/task.service';
import { UserService } from 'src/app/modules/shared/services/user.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { TaskListComponent } from '../task-list/task-list.component';
import { SnackBarService } from 'src/app/modules/shared/services/snack-bar.service';
import { takeUntil } from 'rxjs/operators';
import { CustomValidators } from 'src/app/modules/shared/validators/custom-validators';
import { UpdateTask } from 'src/app/modules/shared/models/task/update-task';
import { NewTask } from 'src/app/modules/shared/models/task/new-task';
import { TaskState } from 'src/app/modules/shared/models/enums/taskState';

@Component({
  selector: 'app-task-new-edit',
  templateUrl: './task-new-edit.component.html',
  styleUrls: ['./task-new-edit.component.css']
})
export class TaskNewEditComponent implements OnInit, OnDestroy, ComponentCanDeactivate {
  public task: Task = {} as Task;
  public states = TaskState;
  public projects: Project[];
  public users: User[];
  public taskForm: FormGroup;

  public isEdit = false;
  public isSaved = false;

  private unsubscribe$ = new Subject<void>();
  constructor(
    private projectService: ProjectService,
    private taskService: TaskService,
    private userService: UserService,
    private router: Router,
    private route: ActivatedRoute,
    private taskListComponent: TaskListComponent,
    private snackBarService: SnackBarService
    ) { }
  public canDeactivate(): boolean | Observable<boolean> {
    return !this.taskForm.dirty || this.isSaved;
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  ngOnInit(): void {
    this.getUsers();
    this.getProjects();

    this.taskForm = new FormGroup({
      name: new FormControl(this.task.name, [
          Validators.required,
          Validators.minLength(4),
          Validators.maxLength(100)
        ]),
        description: new FormControl(this.task.description),
        state: new FormControl(this.task.state),
        finishedAt: new FormControl('', [
          Validators.required,
          CustomValidators.dateMinimum(new Date().toLocaleDateString('en-CA'))
        ]),
        project: new FormControl(this.projects, Validators.required),
        performer: new FormControl(this.users, Validators.required)
    });

    this.route.params
      .subscribe(
        (params: Params) => {
            const id = +params[`id`];
            this.isEdit = Boolean(id);
            if (this.isEdit){
              this.getTask(+params[`id`]);
            }
        }
      );
  }

  public getTask(id: number): void {
    this.taskService
      .getTask(id)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        (resp) => {
          this.task = resp.body;
          this.taskForm.controls[`finishedAt`]
            .setValue(new Date(this.task.finishedAt)
            .toLocaleDateString('en-CA'));
        },
        (error) => {
          this.snackBarService.showErrorMessage(error.message);
          this.onCancel();
        }
      );
  }

  public getUsers(): void {
    this.userService
      .getUsers()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        (resp) => {
          this.users = resp.body;
        },
        (error) => {
          this.snackBarService.showErrorMessage(error.message);
          this.onCancel();
        }
      );
  }

  public getProjects(): void {
    this.projectService
      .getProjects()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        (resp) => {
          this.projects = resp.body;
        },
        (error) => {
          this.snackBarService.showErrorMessage(error.message);
          this.onCancel();
        }
      );
  }

  public onSubmit(): void{
    if (this.isEdit){
      this.taskService
        .updateTask(this.task as UpdateTask)
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe(
          (resp) => {
            const index = this.taskListComponent.tasks.findIndex(p => p.id === resp.body.id);
            this.taskListComponent.tasks[index] = resp.body;
            this.isSaved = true;
          },
          (error) => (this.snackBarService.showErrorMessage(error.message))
        )
        .add(() => (this.onCancel()));
    }
    else {
      this.taskService
      .createTask(this.task as NewTask)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        (resp) => {
            this.taskListComponent.tasks.push(resp.body);
            this.isSaved = true;
            this.onSubmitNavigate(resp.body.id);
        },
        (error) => {
          this.snackBarService.showErrorMessage(error.message);
          this.onCancel();
        }
      );
    }
  }

  public onSubmitNavigate(id: number): void{
    this.router.navigate([`../${id}`], {relativeTo: this.route});
  }

  public onCancel(): void{
    this.router.navigate(['../'], {relativeTo: this.route});
  }
}
