import { Component, OnInit, OnDestroy } from '@angular/core';
import { Team } from 'src/app/modules/shared/models/team/team';
import { ComponentCanDeactivate } from 'src/app/modules/shared/guards/pending-changes.guard';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { Subject, Observable } from 'rxjs';
import { TeamService } from 'src/app/modules/shared/services/team.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { TeamListComponent } from '../team-list/team-list.component';
import { SnackBarService } from 'src/app/modules/shared/services/snack-bar.service';
import { takeUntil } from 'rxjs/operators';
import { UpdateTeam } from 'src/app/modules/shared/models/team/update-team';
import { NewTeam } from 'src/app/modules/shared/models/team/new-team';

@Component({
  selector: 'app-team-new-edit',
  templateUrl: './team-new-edit.component.html',
  styleUrls: ['./team-new-edit.component.css']
})
export class TeamNewEditComponent implements OnInit, OnDestroy, ComponentCanDeactivate {
  public team: Team = {} as Team;
  public teamForm: FormGroup;

  public isEdit = false;
  public isSaved = false;

  private unsubscribe$ = new Subject<void>();
  constructor(
    private teamService: TeamService,
    private router: Router,
    private route: ActivatedRoute,
    private teamListComponent: TeamListComponent,
    private snackBarService: SnackBarService
    ) { }
  public canDeactivate(): boolean | Observable<boolean> {
    return !this.teamForm.dirty || this.isSaved;
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  ngOnInit(): void {
    this.teamForm = new FormGroup({
      name: new FormControl(this.team.name, [
          Validators.required,
          Validators.minLength(4),
          Validators.maxLength(100)
        ])
    });

    this.route.params
      .subscribe(
        (params: Params) => {
            const id = +params[`id`];
            this.isEdit = Boolean(id);
            if (this.isEdit){
              this.getTeam(id);
            }
        }
      );
  }

  public getTeam(id: number): void {
    this.teamService
      .getTeam(id)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        (resp) => {
          this.team = resp.body;
        },
        (error) => {
          this.snackBarService.showErrorMessage(error.message);
          this.onCancel();
        }
      );
  }

  public onSubmit(): void{
    if (this.isEdit){
      this.teamService
        .updateTeam(this.team as UpdateTeam)
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe(
          (resp) => {
            const index = this.teamListComponent.teams.findIndex(p => p.id === resp.body.id);
            this.teamListComponent.teams[index] = resp.body;
            this.isSaved = true;
          },
          (error) => (this.snackBarService.showErrorMessage(error.message))
        )
        .add(() => (this.onCancel()));
    }
    else {
      this.teamService
      .createTeam(this.team as NewTeam)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        (resp) => {
            this.teamListComponent.teams.push(resp.body);
            this.isSaved = true;
            this.onSubmitNavigate(resp.body.id);
        },
        (error) => {
          this.snackBarService.showErrorMessage(error.message);
          this.onCancel();
        }
      );
    }
  }

  public onSubmitNavigate(id: number): void{
    this.router.navigate([`../${id}`], {relativeTo: this.route});
  }

  public onCancel(): void{
    this.router.navigate(['../'], {relativeTo: this.route});
  }
}
