import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Team } from 'src/app/modules/shared/models/team/team';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-team-link',
  templateUrl: './team-link.component.html',
  styleUrls: ['./team-link.component.css']
})
export class TeamLinkComponent implements OnDestroy {
  @Input() team: Team;

  private unsubscribe$ = new Subject<void>();

  constructor() { }
  public ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
