import { Component, OnInit, OnDestroy } from '@angular/core';
import { Team } from 'src/app/modules/shared/models/team/team';
import { Subject } from 'rxjs';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { TeamService } from 'src/app/modules/shared/services/team.service';
import { ProjectService } from 'src/app/modules/shared/services/project.service';
import { TeamListComponent } from '../team-list/team-list.component';
import { SnackBarService } from 'src/app/modules/shared/services/snack-bar.service';
import { takeUntil } from 'rxjs/operators';
import { Project } from 'src/app/modules/shared/models/project/project';

@Component({
  selector: 'app-team-info',
  templateUrl: './team-info.component.html',
  styleUrls: ['./team-info.component.css']
})
export class TeamInfoComponent implements OnInit, OnDestroy {
  public team: Team;
  public projects: Project[];

  private unsubscribe$ = new Subject<void>();

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private projectService: ProjectService,
    private teamService: TeamService,
    private teamListComponent: TeamListComponent,
    private snackBarService: SnackBarService
    ) { }
  ngOnInit(): void {
    this.route.params
      .subscribe(
        (params: Params) => {
            const id: string = params[`id`];
            this.getProjects(+id);
            this.getTeam(+id);
            this.scroll(id);
        }
      );
  }

  public scroll(id: string): void{
    const element = document.getElementById(id);
    const offset = element.getBoundingClientRect().top - document.body.getBoundingClientRect().top - 165;
    window.scrollTo({
      top: offset,
      behavior: 'smooth'
    });
  }

  public getTeam(id: number): void {
    this.teamService
      .getTeam(id)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        (resp) => {
          this.team = resp.body;
        },
        (error) => {
          this.snackBarService.showErrorMessage(error.message);
          this.router.navigate(['../'], {relativeTo: this.route});
        }
      );
  }

  public getProjects(id: number): void {
    this.projectService
      .getProjects()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        (resp) => {
          this.projects = resp.body.filter(p => p.teamId === id);
        },
        (error) => {
          this.snackBarService.showErrorMessage(error.message);
          this.router.navigate(['../'], {relativeTo: this.route});
        }
      );
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  public onEditTeam(): void{
    this.router.navigate(['edit'], {relativeTo: this.route});
  }

  public onDeleteTeam(): void{
    this.teamService
      .deleteTeam(this.team.id)
      .pipe(takeUntil(this.unsubscribe$))
        .subscribe(
          (resp) => {
            this.teamListComponent.teams = this.teamListComponent.teams.filter(t => t.id !== this.team.id);
            this.snackBarService.showUsualMessage(`Team(id: ${this.team.id}) deleted`);
          },
          (error) => (this.snackBarService.showErrorMessage(error.message))
        )
        .add(() => (this.router.navigate(['../'], {relativeTo: this.route})));
  }
}
